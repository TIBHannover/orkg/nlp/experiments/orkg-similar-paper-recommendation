FROM python:3.9
LABEL maintainer="Vlad Nechakhin <vladyslav.nechakhin@l3s.de>"

WORKDIR /orkg-similar-paper-recommendation
COPY requirements.txt /orkg-similar-paper-recommendation/requirements.txt

RUN pip install --upgrade pip
RUN pip install --no-cache-dir --upgrade -r /orkg-similar-paper-recommendation/requirements.txt

COPY ./data /orkg-similar-paper-recommendation/data
COPY ./src /orkg-similar-paper-recommendation/src

CMD ["uvicorn", "src.recommendation_api.papers_service:app", "--host", "0.0.0.0",  "--port", "8000"]
