# ORKG Similar Paper Recommendation

REST API for similar paper recommendation service.

## Prerequisites

We require a python version `3.9` or above.

## How to run

### With ``docker-compose``

Clone the repository:

```commandline
git clone https://gitlab.com/TIBHannover/orkg/orkg-similar-paper-recommendation.git
```

Then compose docker containers:

```commandline
docker-compose build
docker-compose up -d
docker-compose logs -f api 
```

## API Documentation
After successfully running the application, check the documentation at `localhost:8000/docs`
or `localhost:8000/redoc`.

| Endpoint                                | Description                                                                                              |
|-----------------------------------------|----------------------------------------------------------------------------------------------------------|
| **localhost:8000/**                     | Root                                                                                                     |
| **/update**                             | Update the database with recommended papers for every ORKG comparison                                    |
| **/static/{comparison_resource_id}**    | Get recommended papers for a comparison from internal database. Accepts a comparison ID (ex: 'R146851'). |
| **/dynamic/{comparison_resource_id}**   | Get recommended papers for a comparison from Semantic Scholar. Accepts a comparison ID                   |


## License
[MIT](./LICENSE)
