from src.data.search.offline_update import update_recommendation_database
from src.data.search.online_dynamic_search import find_similar_papers_dynamic_mode
from src.data.search.online_static_search import find_similar_papers_static_mode


def main():
    # Update the database with recommended papers for every ORKG comparison (can run for several hours)
    update_recommendation_database(update_every_comparison=True)

    # Get recommended papers for a comparison from Semantic Scholar
    comparison_resource_id = "R146851"
    df_dynamic = find_similar_papers_dynamic_mode(comparison_resource_id)
    print(df_dynamic)

    # Get recommended papers for a comparison from internal database
    df_static = find_similar_papers_static_mode(comparison_resource_id)
    print(df_static)


if __name__ == '__main__':
    main()
