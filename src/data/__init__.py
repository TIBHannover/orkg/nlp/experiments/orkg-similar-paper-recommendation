import os
from dotenv import load_dotenv

load_dotenv()

TRIPLE_STORE_URL = os.environ.get("ORKG_SIMILAR_PAPER_RECOMMENDATION_TRIPLE_STORE_URL", "https://orkg.org/triplestore")
CURRENT_DIR = os.path.dirname(os.path.realpath(__file__))
DATA_DIR = os.path.join(CURRENT_DIR, '..', '..', 'data')
RAW_DATA_DIR = os.path.join(DATA_DIR, 'raw')
PROCESSED_DATA_DIR = os.path.join(DATA_DIR, 'processed')

POSTGRES_DB = os.environ.get('ORKG_SIMILAR_PAPER_RECOMMENDATION_POSTGRES_DB'),
POSTGRES_USER = os.environ.get('ORKG_SIMILAR_PAPER_RECOMMENDATION_POSTGRES_USER'),
POSTGRES_PASSWORD = os.environ.get('ORKG_SIMILAR_PAPER_RECOMMENDATION_POSTGRES_PASSWORD'),
POSTGRES_HOST = os.environ.get('ORKG_SIMILAR_PAPER_RECOMMENDATION_POSTGRES_HOST'),
POSTGRES_PORT = os.environ.get('ORKG_SIMILAR_PAPER_RECOMMENDATION_POSTGRES_PORT')
