import os

import pandas as pd
import psycopg2

from src.data import POSTGRES_DB, POSTGRES_USER, POSTGRES_PASSWORD, POSTGRES_HOST, POSTGRES_PORT


def create_connection():
    conn_string = "dbname={0} user={1} password={2} host={3} port={4}".format(
        POSTGRES_DB,
        POSTGRES_USER,
        POSTGRES_PASSWORD,
        POSTGRES_HOST,
        POSTGRES_PORT
    )
    conn_string = "dbname=similar_paper user=similar_paper password=similar_paper host=postgres port=5432"
    # conn_string = os.environ.get("ORKG_SIMILAR_PAPER_RECOMMENDATION_POSTGRES_CONNECTION")
    conn = psycopg2.connect(conn_string)
    return conn


def execute_query(sql, data=None):
    conn = create_connection()
    cursor = None
    try:
        cursor = conn.cursor()
        # print(sql)
        if data is None:
            cursor.execute(sql)
        else:
            cursor.execute(sql, data)
        conn.commit()
        return cursor
    except Exception as e:
        print(e)
    return cursor


def create_tables():
    create_table_sql = """
            CREATE TABLE IF NOT EXISTS similar_papers (
                paper_id SERIAL PRIMARY KEY,
                comparison_resource_id TEXT NOT NULL,
                score REAL,
                title TEXT,
                abstract TEXT
            )
            """
    execute_query(create_table_sql)


def clear_old_data_from_table():
    delete_rows_sql = "DELETE from similar_papers;"
    execute_query(delete_rows_sql)


def write_dataframe_to_database(df):
    conn = create_connection()
    cursor = conn.cursor()
    # df.to_sql("similar_papers", conn, if_exists='replace', index=False)

    tuples = [tuple(x) for x in df.to_numpy()]
    print("Inserting {0} rows...".format(len(tuples)))
    sql = "INSERT INTO similar_papers(comparison_resource_id, score, title, abstract) VALUES (%s, %s, %s, %s)"
    cursor.executemany(sql, tuples)
    conn.commit()
    conn.close()


def print_database_content():
    select_all_sql = "SELECT * FROM similar_papers;"
    cur = execute_query(select_all_sql)
    rows = cur.fetchall()
    for row in rows:
        print(row)


def print_database_count():
    select_count_sql = "SELECT COUNT(*) FROM similar_papers;"
    cur = execute_query(select_count_sql)
    count = cur.fetchone()
    print(count)


def drop_table():
    drop_table_sql = "DROP TABLE similar_papers;"
    execute_query(drop_table_sql)


def drop_comparison_rows(comparison_resource_id):
    delete_row_sql = "DELETE FROM similar_papers WHERE comparison_resource_id = %s;"
    execute_query(delete_row_sql, (comparison_resource_id,))


def get_recommendation_from_database(comparison_resource_id):
    select_row_sql = """SELECT comparison_resource_id, score, title, abstract 
                        FROM similar_papers WHERE comparison_resource_id=%s;"""
    cur = execute_query(select_row_sql, (comparison_resource_id,))
    recommendations = cur.fetchall()

    recommendations_df = pd.DataFrame(recommendations)
    columns = ["Comparison resource ID", "Score", "Title", "Abstract"]
    recommendations_df.columns = columns
    return recommendations_df


def get_current_time():
    time_sql = """SELECT NOW();"""
    cur = execute_query(time_sql)
    time = cur.fetchone()
    return time


if __name__ == '__main__':
    print(get_current_time())
    # drop_table()
    # create_tables()
    # print_database_content()
    # print(drop_comparison_rows("R107676"))
    # clear_old_data_from_table()
    # print_database_content()
    # print_database_count()
    # drop_comparison_rows("R109612")
    # get_recommendation_from_database("R109612")
