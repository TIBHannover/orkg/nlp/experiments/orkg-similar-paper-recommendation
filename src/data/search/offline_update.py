"""
Update the database with recommended papers for each of the ORKG comparison, to be run periodically
"""
import time

import pandas as pd

from src.data.dataset_update.database_postgres import write_dataframe_to_database, clear_old_data_from_table, \
    print_database_content, create_tables, print_database_count
from src.data.search.semantic_scholar_api import get_ss_papers_by_doi, convert_ss_papers_to_dataframe
from src.data.search.similar_paper_search import create_query, create_index, insert_data_into_es_index, query_es_index, \
    delete_index
from src.data.sparql.fetch_orkg_data import get_paper_doi_from_comparison, get_all_comparisons


def get_all_comparison_resource_list():
    comparison_df = get_all_comparisons()
    print(str(len(comparison_df["comparison"])) + " comparisons with at least 1 contribution")
    comparison_resource_list = []
    for comparison_resource in comparison_df["comparison"]:  # resource example: 'http://orkg.org/orkg/resource/R25358'
        comparison_resource_list.append(comparison_resource.split('/')[-1])
    return comparison_resource_list


def wait_for_ss_api_limits(api_timeout_start):
    time_elapsed = time.time() - api_timeout_start
    if time_elapsed < 3:
        time.sleep(3 - time_elapsed)
        print("Sleeping for {0} seconds".format(3 - time_elapsed))


def update_recommendation_database(update_every_comparison=True):
    if update_every_comparison:
        # Getting a list of all ORKG comparisons
        comparison_resource_list = get_all_comparison_resource_list()
    else:
        # Get only first 3 comparisons for testing purposes
        comparison_resource_list = get_all_comparison_resource_list()[:3]

    result_dataframe_list = list()
    count = 0
    for comparison_resource_id in comparison_resource_list:
        count += 1
        print("{0}/{1} ({2:.2f}%). Comparison: {3}".format(count,
                                                           len(comparison_resource_list),
                                                           (count / len(comparison_resource_list)) * 100,
                                                           comparison_resource_id))

        # For each comparison - getting a list of paper DOIs
        doi_list = get_paper_doi_from_comparison(comparison_resource_id)
        if len(doi_list) < 1:
            print("Not enough DOI to recommend papers for comparison {0}".format(comparison_resource_id))
            continue

        # Getting recommended papers from semantic scholar
        recommended_paper_names = get_ss_papers_by_doi(doi_list)
        if len(recommended_paper_names) < 1:
            print("No recommendations found for comparison {0}".format(comparison_resource_id))
            wait_for_ss_api_limits(time.time())
            continue

        recommended_dataframe = convert_ss_papers_to_dataframe(recommended_paper_names)
        api_timeout_start = time.time()

        # Getting the ORKG contributions (names + values)
        search_query = create_query(comparison_resource_id)

        # Rerank the results by elastic search
        es_index = comparison_resource_id.lower()
        create_index(es_index)
        insert_data_into_es_index(recommended_dataframe, es_index)
        try:
            recommended_df = query_es_index(search_query, comparison_resource_id, es_index)
        except:
            print("Failed to parse query")
            continue
        result_dataframe_list.append(recommended_df)
        delete_index(es_index)

        # Wait for Sematic Scholar API limit to reset (100 requests per 5 minutes or 1 request per 3 seconds)
        wait_for_ss_api_limits(api_timeout_start)

    # Write the results to the database
    result_dataframe = pd.concat(result_dataframe_list)
    create_tables()
    clear_old_data_from_table()
    write_dataframe_to_database(result_dataframe)
    # print_database_content()


def main():
    update_recommendation_database()


if __name__ == '__main__':
    main()
