"""
Get a list of similar papers for ORKG comparison dynamically (upon user's request) by querying Semantic Scholar
"""
import time

from src.data.dataset_update.database_postgres import drop_comparison_rows, write_dataframe_to_database, create_tables
from src.data.search.semantic_scholar_api import get_ss_papers_by_doi, convert_ss_papers_to_dataframe
from src.data.search.similar_paper_search import create_index, insert_data_into_es_index, query_es_index, \
    create_query
from src.data.sparql.fetch_orkg_data import get_paper_doi_from_comparison


def find_similar_papers_dynamic_mode_timed(comparison_resource_id):
    start_time = time.time()

    # Get a list of doi form comparison by id form orkg
    doi_list = get_paper_doi_from_comparison(comparison_resource_id)
    orkg_paper_query_time = time.time()

    # Getting the ORKG contributions (names + values)
    search_query = create_query(comparison_resource_id)
    orkg_search_query_time = time.time()

    # Use semantic scholar api to get a similar paper recommendation
    ss_recommended_paper_names = get_ss_papers_by_doi(doi_list)
    ss_recommended_dataframe = convert_ss_papers_to_dataframe(ss_recommended_paper_names)
    ss_query_time = time.time()

    # Create elastic search index
    es_index = comparison_resource_id.lower()
    create_index(es_index)
    insert_data_into_es_index(ss_recommended_dataframe, es_index)
    es_index_time = time.time()

    # Getting similar papers from external dataset
    recommended_df = query_es_index(search_query, comparison_resource_id, es_index)

    # Updating the database with up-to-date papers
    drop_comparison_rows(comparison_resource_id)
    write_dataframe_to_database(recommended_df)

    end_time = time.time()
    print('ORKG paper query time: {:.2f} seconds'.format(orkg_paper_query_time - start_time))  # 0.10 seconds
    print(
        'ORKG search query time: {:.2f} seconds'.format(orkg_search_query_time - orkg_paper_query_time))  # 0.57 seconds
    print('Semantic scholar query time: {:.2f} seconds'.format(ss_query_time - orkg_search_query_time))  # 4.30 seconds
    print('Elastic search indexing time: {:.2f} seconds'.format(es_index_time - ss_query_time))  # 1.29 seconds
    print('Elastic search query time: {:.2f} seconds'.format(end_time - es_index_time))  # 0.97 seconds
    print('Total execution time: {:.2f} seconds'.format(end_time - start_time))  # 7.24 seconds
    return recommended_df


def find_similar_papers_dynamic_mode(comparison_resource_id):
    # Get a list of doi form comparison by id form orkg
    doi_list = get_paper_doi_from_comparison(comparison_resource_id)

    # Getting the ORKG contributions (names + values)
    search_query = create_query(comparison_resource_id)

    # Use semantic scholar api to get a similar paper recommendation
    ss_recommended_paper_names = get_ss_papers_by_doi(doi_list)
    ss_recommended_dataframe = convert_ss_papers_to_dataframe(ss_recommended_paper_names)

    # Create elastic search index
    es_index = comparison_resource_id.lower()
    create_index(es_index)
    insert_data_into_es_index(ss_recommended_dataframe, es_index)

    # Getting similar papers from external dataset
    recommended_df = query_es_index(search_query, comparison_resource_id, es_index)

    # Updating the database with up-to-date papers
    create_tables()
    drop_comparison_rows(comparison_resource_id)
    write_dataframe_to_database(recommended_df)
    return recommended_df


def main():
    comparison_resource_id = "R146851"
    find_similar_papers_dynamic_mode(comparison_resource_id)


if __name__ == '__main__':
    main()
