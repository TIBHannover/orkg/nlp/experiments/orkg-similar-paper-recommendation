"""
Get a list (dataframe) of similar papers from a database for ORKG comparison for static display
"""
import time

from src.data.dataset_update.database_postgres import get_recommendation_from_database


def find_similar_papers_static_mode_timed(comparison_resource_id):
    start_time = time.time()
    recommendation_df = get_recommendation_from_database(comparison_resource_id)
    end_time = time.time()
    print('Total execution time: {:.4f} seconds'.format(end_time - start_time))  # 0.0020 seconds
    return recommendation_df


def find_similar_papers_static_mode(comparison_resource_id):
    recommendation_df = get_recommendation_from_database(comparison_resource_id)
    return recommendation_df


def main():
    comparison_resource_id = "R140463"
    find_similar_papers_static_mode(comparison_resource_id)


if __name__ == '__main__':
    main()
