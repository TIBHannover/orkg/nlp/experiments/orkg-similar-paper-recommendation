import requests
import pandas as pd


def get_ss_papers_by_doi(doi_list):
    # api-endpoint
    url = "https://api.semanticscholar.org/recommendations/v1/papers/?limit=500;fields=title,abstract,externalIds"

    # data to be sent to api
    json = {"positivePaperIds": doi_list}

    # sending get request and saving the response as response object
    r = requests.post(url=url, json=json)

    # extracting data in json format
    # print(r.url)
    # print(r.text)
    data = r.json()
    try:
        recommended_papers = data["recommendedPapers"]
        return recommended_papers
    except KeyError:
        print("Paper with id not found")
        return []


def convert_ss_papers_to_dataframe(recommended_papers_list):
    result_list = []
    for i in range(len(recommended_papers_list)):
        recommendation = [recommended_papers_list[i]["title"],
                          recommended_papers_list[i]["abstract"]]
        result_list.append(recommendation)
    result_df = pd.DataFrame(result_list)
    result_df.columns = [
        "Title",
        "Abstract"]
    return result_df


def get_paper_names_recommendation_by_doi(doi, recommendations_per_paper=5):
    # api-endpoint
    url = "https://api.semanticscholar.org/recommendations/v1/papers/forpaper/"
    r = requests.get(url=url + doi)

    # extracting data in json format
    data = r.json()
    try:
        recommended_papers = data["recommendedPapers"]
    except KeyError:
        print("Paper with id not found")
        return []
    paper_names = []
    for i in range(min(len(recommended_papers), recommendations_per_paper)):
        paper_names.append(recommended_papers[i]["title"])
    return paper_names
