import os
import re
import time

import pandas as pd
from elasticsearch import Elasticsearch

from src.data import TRIPLE_STORE_URL, PROCESSED_DATA_DIR
from src.data.sparql.queries import PROPERTIES_FROM_COMPARISON, TOP_5_CONTRIBUTIONS_FROM_COMPARISON
from src.data.sparql.sparql_service import query

# es = Elasticsearch(hosts=['http://localhost:9200'], verify_certs=False)  # local elastic search
es = Elasticsearch(
    hosts=[
        os.getenv(
            "ORKG_SIMILAR_PAPER_RECOMMENDATION_ELASTIC_SEARCH_HOST",
            "http://localhost:9200",
        )
    ],
    verify_certs=False)  # docker elastic search
INDEX_NAME = "semantic_scholar_papers_index"


def create_index(index_name):
    # print(es.ping())
    # print(es.indices.exists(index=index_name))
    es.indices.create(index=index_name, ignore=400)
    # print("Index {0} exists: {1}".format(index_name, es.indices.exists(index=index_name)))


def index_exists(index_name):
    print("Index {0} exists: {1}".format(index_name, es.indices.exists(index=index_name)))
    return es.indices.exists(index=index_name)


def recreate_index(index_name):
    es.indices.delete(index=index_name, ignore=[400, 404])
    es.indices.create(index=index_name, ignore=400)
    print("Index {0} exists: {1}".format(index_name, es.indices.exists(index=index_name)))


def delete_index(index_name):
    es.indices.delete(index=index_name, ignore=[400, 404])


def insert_data_into_es_index(df, index_name):
    for index, row in df.iterrows():
        paper_dict = {"Title": row["Title"], "Abstract": row["Abstract"]}
        es.index(index=index_name, id=index, body=paper_dict)
        print("{0}/{1} ({2:.2f}%)".format(index + 1, len(df.index), (index + 1) * 100 / len(df.index)))


def clean_up_data(file_name):
    """
    Remove papers without abstract and duplicate papers from the dataset
    :param file_name: dataset name
    """
    df = read_dataset(file_name)
    # removing rows with empty abstracts
    df = df[df['Abstract'].notna()]
    # remove duplicates
    df = df.drop_duplicates(subset=['DOI'], keep='first')
    write_dataset(df, file_name)


def show_duplicates(file_name):
    df = read_dataset(file_name)
    df = df.groupby(['DOI', 'Title']).size().reset_index(name='count')
    print(df)


def read_dataset(file_name):
    return pd.read_csv(file_name + ".csv", encoding='utf-8', sep=';')


def write_dataset(data_frame, file_name):
    data_frame.to_csv(file_name + ".csv", encoding='utf-8', sep=';', index=False)  # , decimal=","


def create_query(comparison_resource):
    """
    Combine the paper names and property values of first 5 contributions for a given comparison
    :param comparison_resource: The resource of an ORKG comparison (e.g. "R146851")
    :return: a string representation of a comparison for an elastic search query
    """
    comparison_contributions = query(TRIPLE_STORE_URL, TOP_5_CONTRIBUTIONS_FROM_COMPARISON(comparison_resource))
    property_list = comparison_contributions["paper_title"].to_list()

    for contribution_id in comparison_contributions["contribution"]:
        contribution_id = contribution_id.split('/')[-1]
        comparison_papers = query(TRIPLE_STORE_URL, PROPERTIES_FROM_COMPARISON(contribution_id))
        property_list.extend(comparison_papers["property_value"].to_list())
    property_string = ' '.join(property_list)

    # escaping special characters to avoid elasticsearch.BadRequestError due to using reserved characters
    property_string = re.escape(property_string)
    property_string = property_string.replace("\ ", " ")
    property_string = property_string.replace("/", " ")
    property_string = property_string.replace(":", " ")
    property_string = property_string.replace(">", " ")
    property_string = property_string.replace("<", " ")
    property_string = property_string[:2500] if len(property_string) > 75 else property_string  # truncate long strings

    # print(property_string)
    return property_string


def get_search_result(search_query):
    res = es.search(index=INDEX_NAME, q=search_query, size=50)
    return res


def query_es_index(search_query, comparison_resource_id, es_index=INDEX_NAME):
    """
    Query the elastic search index to get a dataframe of recommended papers
    :param search_query:
    :param comparison_resource_id:
    :param es_index:
    :return: dataframe of recommended papers
    """
    res = es.search(index=es_index, q=search_query, size=50)

    data = {"Comparison resource ID": [], "Score": [], "Title": [], "Abstract": []}
    for result in res.body["hits"]["hits"]:
        data["Comparison resource ID"].append(comparison_resource_id)
        data["Score"].append(result["_score"])
        data["Title"].append(result["_source"]["Title"])
        data["Abstract"].append(get_abstract_by_id(result["_id"], es_index))
    df = pd.DataFrame(data=data)
    return df


def get_abstract_by_id(elastic_id, es_index):
    # Getting abstract by id since long strings get ignored during search
    res = es.get(index=es_index, id=elastic_id)
    abstract = res["_source"]["Abstract"]
    return abstract
