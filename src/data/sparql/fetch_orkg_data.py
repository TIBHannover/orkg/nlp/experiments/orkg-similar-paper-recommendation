from src.data import TRIPLE_STORE_URL
from src.data.sparql.queries import *
from src.data.sparql.sparql_service import query


def get_comparisons_over_three_papers():
    return query(TRIPLE_STORE_URL, COMPARISONS_OVER_THREE_PAPERS)


def get_all_comparisons():
    return query(TRIPLE_STORE_URL, ALL_COMPARISONS)


def get_paper_doi_from_comparison(comparison_id):
    comparison_papers = query(TRIPLE_STORE_URL, PAPER_DOI_FROM_COMPARISON(comparison_id))
    # remove papers without doi
    doi_list = []
    for i in comparison_papers["doi"]:
        if i != "":
            doi_list.append(i)
    return list(set(doi_list))


def get_paper_title_from_comparison(comparison_id):
    comparison_papers = query(TRIPLE_STORE_URL, PAPER_DOI_FROM_COMPARISON(comparison_id))
    # remove papers without doi
    title_list = []
    for i in range(len(comparison_papers)):
        if comparison_papers["doi"][i] != "":
            title_list.append(comparison_papers["paper_title"][i])
    return list(set(title_list))
