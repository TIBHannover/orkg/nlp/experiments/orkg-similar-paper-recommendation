# used for creating an evaluation set

COMPARISONS_OVER_THREE_PAPERS = """
PREFIX orkgp: <http://orkg.org/orkg/predicate/>
PREFIX orkgc: <http://orkg.org/orkg/class/>

SELECT ?comparison (COUNT(?contribution) AS ?contribution_count)
WHERE {
  ?comparison rdf:type orkgc:Comparison;
              orkgp:compareContribution ?contribution.
}
GROUP BY ?comparison
HAVING(COUNT(?contribution) > 3)
"""

ALL_COMPARISONS = """
PREFIX orkgp: <http://orkg.org/orkg/predicate/>
PREFIX orkgc: <http://orkg.org/orkg/class/>

SELECT ?comparison (COUNT(?contribution) AS ?contribution_count)
WHERE {
  ?comparison rdf:type orkgc:Comparison;
              orkgp:compareContribution ?contribution.
}
GROUP BY ?comparison
HAVING(COUNT(?contribution) > 0)
"""


# used for semantic scholar recommendation
def PAPER_DOI_FROM_COMPARISON(comparison):
    return """
        PREFIX orkgr: <http://orkg.org/orkg/resource/>
        PREFIX orkgp: <http://orkg.org/orkg/predicate/>
        PREFIX orkgc: <http://orkg.org/orkg/class/>

        SELECT ?paper_title ?paper ?doi
        WHERE {{
            orkgr:{} rdf:type orkgc:Comparison;
                orkgp:compareContribution ?contribution.
            ?paper orkgp:P31 ?contribution;
                orkgp:P26 ?doi;
                rdfs:label ?paper_title.
        }}""".format(comparison)


# used by Elasticsearch to get 5 contributions of comparison
def TOP_5_CONTRIBUTIONS_FROM_COMPARISON(comparison):
    return """
PREFIX orkgr: <http://orkg.org/orkg/resource/>
PREFIX orkgp: <http://orkg.org/orkg/predicate/>
PREFIX orkgc: <http://orkg.org/orkg/class/>

SELECT ?contribution ?paper_title
WHERE {{
    BIND(orkgr:{} AS ?comparison_id).
    ?comparison_id rdf:type orkgc:Comparison;
        orkgp:compareContribution ?contribution.
    ?paper orkgp:P31 ?contribution;
                orkgp:P26 ?doi;
                rdfs:label ?paper_title.
}}
LIMIT 5
    """.format(comparison)  # R275000


# used by Elasticsearch to extract all properties from contribution
def PROPERTIES_FROM_COMPARISON(contribution):
    return """
PREFIX orkgr: <http://orkg.org/orkg/resource/>
PREFIX orkgp: <http://orkg.org/orkg/predicate/>
PREFIX orkgc: <http://orkg.org/orkg/class/>

SELECT ?contribution_property ?property_value
WHERE {{
    BIND(orkgr:{} AS ?contribution_id).
  ?contribution_id ?property ?contribution_property.
  ?contribution_property rdfs:label ?property_value.
}}
    """.format(contribution)  # R273049
