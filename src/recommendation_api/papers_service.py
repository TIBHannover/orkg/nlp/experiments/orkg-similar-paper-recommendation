import os

import uvicorn as uvicorn
from fastapi import FastAPI

# from src.data.dataset_update.database_postgres import current_time
from src.data.dataset_update.database_postgres import get_current_time
from src.data.search.offline_update import update_recommendation_database
from src.data.search.online_dynamic_search import find_similar_papers_dynamic_mode
from src.data.search.online_static_search import find_similar_papers_static_mode

app = FastAPI(
    title="ORKG-similar-paper-recommendation",
    root_path=os.getenv("ORKG_SIMILAR_PAPER_RECOMMENDATION_PREFIX", ""),
    servers=[
        {
            "url": os.getenv("ORKG_SIMILAR_PAPER_RECOMMENDATION_PREFIX", ""),
            "description": "",
        }
    ],
)


@app.get("/")
async def root():
    return {"Message": "ORKG Similar Paper Recommendation API"}


@app.get("/update")
def update_database_offline_mode():
    # Update the recommendations for every comparison
    update_recommendation_database(False)
    return {"Update": "Done"}


@app.get("/static/{comparison_resource_id}")
def get_similar_papers_static_mode(comparison_resource_id: str):
    # Get recommended papers for a comparison from internal database
    df_static = find_similar_papers_static_mode(comparison_resource_id)
    return df_static.to_dict()


@app.get("/dynamic/{comparison_resource_id}")
def get_similar_papers_dynamic_mode(comparison_resource_id: str):
    # Get recommended papers for a comparison from Semantic Scholar
    df_dynamic = find_similar_papers_dynamic_mode(comparison_resource_id)
    return df_dynamic.to_dict()


if __name__ == '__main__':
    uvicorn.run(app, port=8000, host="0.0.0.0")  # 127.0.0.1:8000
    # uvicorn src.recommendation_api.papers_service:app --reload
